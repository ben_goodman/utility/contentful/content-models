locals {
    code_snippet_styles = [
        "bash",
        "css",
        "dockerfile",
        "go",
        "graphql",
        "hcl",
        "html",
        "js",
        "json",
        "jsx",
        "makefile",
        "mermaid",
        "markdown",
        "rust",
        "text",
        "tsx",
        "ts",
        "yaml",
        "yml",
    ]
}

resource "contentful_contenttype" "code_snippet" {
    name            = "Code Snippet"
    description     = "A formatted code snippet with an optional title, alt-text, style directive, and code."
    display_field   = "title"
    content_type_id = "CodeSnippet"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [contentful_environment.master]

    field {
        name     = "Title"
        id       = "title"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name     = "Description"
        id       = "description"
        type     = "Symbol"
        required = true
    }

    field {
        name     = "Style"
        id       = "style"
        type     = "Symbol"
        required = true
        validations = [
            jsonencode({
                "in" : local.code_snippet_styles
            })
        ]
    }

    field {
        name     = "Code"
        id       = "code"
        type     = "Text"
        required = true
    }

    field {
        name     = "Caption"
        id       = "caption"
        type     = "Text"
        required = true
    }

}
