# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/regressivetech/contentful" {
  version     = "0.4.0"
  constraints = "0.4.0"
  hashes = [
    "h1:6AMwac0rPW6p0L4Erp1BsScwnoYELCnzlYGn343mGqQ=",
    "zh:09c6fac592f8445b87b4070de408ce903617b31066979d542d7e03c2e6fde8c3",
    "zh:0b44fe265787437e4db0243ae045971e2ca5d38e0c6ac4d5cacbb4b78a43611f",
    "zh:0e6f0ecb4e374a2ea8297847f371a885495779cccab04c9602e4e459e4ce402b",
    "zh:1124e554558c76c80e57b9ba75c29b5671c5dbf9b81cf3bd90f70dd4a02eb99d",
    "zh:3093b26dacfee8865cdf5b867956bfc48517ce6581273392eb9c247688afeb77",
    "zh:69d504b2368a620c4fdc0da526175d2f2e4e281cf2502e3f0f2d910baeab5b2b",
    "zh:8dd9d225387511abda6e3a92df7e451990e4fde85b0725759ec37acd91ff28a8",
    "zh:9ff0ec3c6944e903b052880694472570aa94fb8cc2a43f1885ecf0b4dde3ed90",
    "zh:b4f0434ee1a28bd9034ba8ee60fc37e09b16898bba5710122088c1799e87f74a",
    "zh:ca8d76a39193857d97ab08123f80ddc73ab263ea9f442b9ca0892c0952549850",
    "zh:e096fafc0ab3f34e5afade18a09074fd2a278a11751cb64263b6908696e09700",
    "zh:e5c793ab54f36e7bdb2a24e6dc79635778e1e635bdff179375630fe676411086",
    "zh:ea5f8b8ffd7b6d53e87d6e6586d0ea366b4f6daacdd4d1eace176cb425808ee3",
  ]
}
