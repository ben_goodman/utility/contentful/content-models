resource "contentful_contenttype" "site_header" {
  name            = "Site Header"
  description     = "A website header containing: title, subtitle, contact ref"
  display_field   = "title"
  content_type_id = "SiteHeader"

  space_id = contentful_space.blog.id
  env_id   = contentful_environment.master.id
  depends_on = [
    contentful_environment.master,
    contentful_contenttype.fancy_link
  ]

  field {
    name     = "Title"
    id       = "title"
    type     = "Symbol"
    required = true
    validations = [
      local.field_validations.make_unique
    ]
  }

  field {
    name        = "Subtitle"
    id          = "subtitle"
    type        = "Symbol"
    required    = true
  }

  field {
    name     = "Navigation Links"
    id       = "navigationLinks"
    type     = "Array"
    required = false
    items {
      type      = "Link"
      link_type = "Entry"
      validations = [
        jsonencode({
          "linkContentType" : [
            contentful_contenttype.fancy_link.id
          ]
        })
      ]
    }
  }

  field {
    name      = "Banner Image"
    id        = "bannerImage"
    type      = "Link"
    link_type = "Asset"
    required  = false
    validations = [
      local.field_validations.image_only
    ]
  }

  field {
    name      = "Default"
    id        = "default"
    type      = "Boolean"
  }
}
