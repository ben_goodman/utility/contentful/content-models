resource "contentful_contenttype" "fancy_link" {
    name            = "Fancy Link"
    description     = "A URL with optional fields for an icon and display text."
    display_field   = "displayText"
    content_type_id = "FancyLink"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [contentful_environment.master]

    field {
        name     = "Display Text"
        id       = "displayText"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name     = "URL"
        id       = "url"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name      = "Icon"
        id        = "icon"
        type      = "Link"
        required  = false
        link_type = "Asset"
        validations = [
            local.field_validations.image_only
        ]
    }
}
