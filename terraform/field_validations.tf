locals {
    field_validations = {
        image_only = jsonencode({
            "linkMimetypeGroup" : [
                "image"
            ]
        }),
        make_unique = jsonencode({
            "unique" : true
        }),
        make_url_safe = jsonencode({
            "regexp" : {
                "pattern" : "^[a-zA-Z0-9_-]*$",
                "flags" : null
            },
            "message" : "Must be a URL-safe string containing only alpha-numeric characters and underscores, hyphens and periods."
        }),
        rich_text = {
            enable_marks = jsonencode({
                "enabledMarks" : [
                    "bold",
                    "italic",
                    "underline",
                    "code",
                    "superscript",
                    "subscript"
                ],
                "message" : "Only bold, italic, underline, code, superscript, and subscript marks are allowed"
            }),
            enable_node_types = jsonencode({
                "enabledNodeTypes" : [
                    "heading-1",
                    "heading-2",
                    "heading-3",
                    "heading-4",
                    "heading-5",
                    "heading-6",
                    "unordered-list",
                    "ordered-list",
                    "blockquote",
                    "hr",
                    "table",
                    "hyperlink",
                    "entry-hyperlink",
                    "asset-hyperlink",
                    "embedded-entry-block",
                    "embedded-entry-inline",
                    "embedded-asset-block"
                ],
                "message" : "Only heading 1, heading 2, heading 3, heading 4, heading 5, heading 6, unordered list, ordered list, quote, horizontal rule, table, link to Url, link to entry, link to asset, block entry, inline entry, and asset nodes are allowed"
            }),
        }

    }
}