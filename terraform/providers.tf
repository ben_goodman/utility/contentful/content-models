terraform {
    required_providers {
        contentful = {
            source  = "regressivetech/contentful"
            version = "0.4.0"
        }
    }
    required_version = ">= 0.13"
}
