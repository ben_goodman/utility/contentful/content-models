#****
# !Article\ Topic[as:h(1)]
# Used for tagging articles with topics.
#
# !Model[as:list]
#  Content Type ID: `"ArticleTopic"`
#  Display Field: `"name"`
#
# !Fields !Display\ Name `"Name"` !Field\ ID `"name"`  !Required "Yes" !Validations "`unique`"

resource "contentful_contenttype" "article_topic" {
    name            = "Article Topic"
    description     = "A topic for an article"
    display_field   = "name"
    content_type_id = "ArticleTopic"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [contentful_environment.master]

    field {
        name     = "Name"
        id       = "name"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }
}