resource "contentful_space" "blog" {
    name           = "blog"
    default_locale = "en-US"
}

resource "contentful_environment" "master" {
    space_id = contentful_space.blog.id
    name     = "master"

    depends_on = [contentful_space.blog]
}
