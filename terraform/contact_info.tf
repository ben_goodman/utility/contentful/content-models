resource "contentful_contenttype" "contact_info" {
    name = "Contact info"
    description = "Contact info including: name, email, a ref to a resume content type, a list of additional urls"
    display_field = "name"
    content_type_id = "ContactInfo"

    space_id = contentful_space.blog.id
    env_id = contentful_environment.master.id
    depends_on = [
        contentful_environment.master,
        contentful_contenttype.fancy_link
    ]

    field {
        name = "Name"
        id = "name"
        type = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name = "Email"
        id = "email"
        type = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name = "Additional URLs"
        id = "additionalUrls"
        type = "Array"
        required = false
        items {
            type = "Link"
            link_type = "Entry"
            validations = [
                jsonencode({
                    "linkContentType" : [
                        contentful_contenttype.fancy_link.id
                    ]
                })
            ]
        }
    }

    field {
        id = "avatar"
        name = "avatar"
        type = "Link"
        link_type = "Asset"
        validations = [
            local.field_validations.image_only
        ]
    }

}