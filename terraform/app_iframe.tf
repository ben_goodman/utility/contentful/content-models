resource "contentful_contenttype" "application_iframe" {
    name            = "Application Iframe"
    description     = "An iframe that displays an application."
    display_field   = "title"
    content_type_id = "ApplicationIframe"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [contentful_environment.master]

    field {
        name     = "Title"
        id       = "title"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name     = "source"
        id       = "source"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }
}