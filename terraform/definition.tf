resource "contentful_contenttype" "definition" {
    name            = "Definition"
    description     = "A definition of a term."
    display_field   = "term"
    content_type_id = "Definition"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [contentful_environment.master]

    field {
        name     = "Term"
        id       = "term"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name = "Abbreviation"
        id   = "abbreviation"
        type = "Boolean"
        required = true
    }

    field {
        name     = "Definition"
        id       = "definition"
        type     = "Text"
        required = false
    }

    field {
        name     = "Definition (Rich Text)"
        id       = "definitionRichText"
        type     = "RichText"
        required = true
    }
}