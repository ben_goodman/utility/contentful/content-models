output "space_id" {
    value = contentful_space.blog.id
}

output "environment_id" {
    value = contentful_environment.master.id
}
