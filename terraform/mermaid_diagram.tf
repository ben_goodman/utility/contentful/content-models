locals {
    mermaid_versions = [
        "10.4.0"
    ]
}

resource "contentful_contenttype" "mermaid_diagram" {
    name            = "Mermaid Diagram"
    description     = "A mermaid diagram with an optional title and alt-text."
    display_field   = "title"
    content_type_id = "MermaidDiagram"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [ contentful_environment.master ]

    field {
        name     = "Title"
        id       = "title"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name     = "Description"
        id       = "description"
        type     = "Symbol"
        required = true
    }

    field {
        name     = "Caption"
        id       = "caption"
        type     = "Text"
        required = true
    }

    field {
        name     = "Diagram"
        id       = "diagram"
        type     = "Text"
        required = true
    }

    field {
        name     = "Mermaid Version"
        id       = "mermaidVersion"
        type     = "Symbol"
        required = true
        validations = [
            jsonencode({
                "in" : local.mermaid_versions
            })
        ]
    }
}