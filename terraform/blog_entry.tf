#****
# !Blog\ Entry[as:h(1)]
# A rich text blog entry with fields for a title, associated topics, etc.
#
# !Model[as:list]
#  Content Type ID: `BlogEntry`
#  Display Field: `longTitle`
#
# !Fields !Display\ Name Canonical Title !Field\ ID `canonicalTitle` !Required Yes  !Validations `unique` `url safe`
# !Fields !Display\ Name Long Title      !Field\ ID `longTitle`      !Required Yes  !Validations `unique`
# !Fields !Display\ Name Subtitle        !Field\ ID `subtitle`       !Required Yes
# !Fields !Display\ Name Thumbnail       !Field\ ID `thumbnail`      !Required No   !Validations `only: image`
# !Fields !Display\ Name Topics          !Field\ ID `topics`         !Required Yes  !Validations `linkContentType: ArticleTopic`
# !Fields !Display\ Name Body            !Field\ ID `body`           !Required Yes

resource "contentful_contenttype" "blog_entry" {
    name            = "Blog Entry"
    description     = "Blog page with title, publish date, optional updated date, body, thumbnail image, other recommended posts, a list of topics"
    display_field   = "longTitle"
    content_type_id = "BlogEntry"

    space_id   = contentful_space.blog.id
    env_id     = contentful_environment.master.id
    depends_on = [
        contentful_environment.master,
        contentful_contenttype.article_topic
    ]

    field {
        name     = "Canonical Title"
        id       = "canonicalTitle"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique,
            local.field_validations.make_url_safe
        ]
    }

    field {
        name     = "Long Title"
        id       = "longTitle"
        type     = "Symbol"
        required = true
        validations = [
            local.field_validations.make_unique
        ]
    }

    field {
        name     = "Subtitle"
        id       = "subtitle"
        type     = "Symbol"
        required = true
    }

    field {
        name      = "Thumbnail"
        id        = "thumbnail"
        type      = "Link"
        link_type = "Asset"
        required = false
        validations = [
            local.field_validations.image_only
        ]
    }

    field {
        name     = "Topics"
        id       = "topics"
        type     = "Array"
        required = true
        items {
            type = "Link"
            link_type = "Entry"
            validations = [
                jsonencode({
                    "linkContentType" : [
                        contentful_contenttype.article_topic.id
                    ]
                })
            ]
        }
    }

    field {
        name      = "Body"
        id        = "body"
        type      = "RichText"
        localized = true
        required  = true
        validations = [
            local.field_validations.rich_text.enable_marks,
            local.field_validations.rich_text.enable_node_types,
            jsonencode({
                "nodes" : {}
            })
        ]
    }

}