terraform {
    backend "s3" {
        bucket = "tf-states-us-east-1-bgoodman"
        key    = "content-models/terraform.tfstate"
        region = "us-east-1"
        encrypt = true
    }
}