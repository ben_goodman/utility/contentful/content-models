resource "contentful_contenttype" "site_footer" {
  name            = "Site Footer"
  description     = "Global footer for the site."
  display_field   = "title"
  content_type_id = "SiteFooter"

  space_id = contentful_space.blog.id
  env_id   = contentful_environment.master.id
  depends_on = [
    contentful_environment.master,
    contentful_contenttype.fancy_link
  ]

  field {
    name     = "Title"
    id       = "title"
    type     = "Symbol"
    required = true
    validations = [
      local.field_validations.make_unique
    ]
  }

  field {
      name      = "Body"
      id        = "body"
      type      = "RichText"
      localized = true
      required  = true
      validations = [
          local.field_validations.rich_text.enable_marks,
          local.field_validations.rich_text.enable_node_types,
      ]
  }

  field {
    name      = "Default"
    id        = "default"
    type      = "Boolean"
  }
}
